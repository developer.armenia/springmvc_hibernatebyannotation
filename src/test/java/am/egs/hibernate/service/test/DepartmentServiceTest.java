package am.egs.hibernate.service.test;

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.bean.EmployeeBean;
import am.egs.hibernate.config.SpringRootConfig;
import am.egs.hibernate.service.DepartmentService;
import am.egs.hibernate.service.EmployeeService;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by haykh on 5/23/2019.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringRootConfig.class})
public class DepartmentServiceTest {

  @Autowired
  private DepartmentService departmentService;

  /*@Test
  public void addUserTest() {
    //1 Department
    DepartmentBean department = new DepartmentBean();
    department.setName("IT Department");
    //3 EmployeeBean
    EmployeeBean employee1 = new EmployeeBean();
    employee1.setName("Adam");
    employee1.setDepartment(department);

    EmployeeBean employee2 = new EmployeeBean();
    employee2.setName("Miller");
    employee2.setDepartment(department);

    EmployeeBean employee3 = new EmployeeBean();
    employee3.setName("Smith");
    employee3.setDepartment(department);
    // add employ into department
    department.getEmployees().add(employee1);
    department.getEmployees().add(employee2);
    department.getEmployees().add(employee3);

    departmentService.addDepartment(department);
  }*/

  @Test
  public void getAllEmployeesTest() {
    List<DepartmentBean> allDepartment = departmentService.getAllDepartment();
    Assert.assertNotNull(allDepartment);
  }

}
