package am.egs.hibernate.dao;

/**
 * Created by haykh on 5/23/2019.
 */
import java.util.List;
import am.egs.hibernate.entity.Employee;

public interface EmployeeDAO {

  public void addEmployee(Employee employee);

  public List<Employee> getAllEmployees();

  public void deleteEmployee(Integer employeeId);

  public Employee updateEmployee(Employee employee);

  public Employee getEmployee(int employeeid);
}

