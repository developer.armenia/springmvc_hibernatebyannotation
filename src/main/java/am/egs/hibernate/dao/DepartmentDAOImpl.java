package am.egs.hibernate.dao;

/**
 * Created by haykh on 5/23/2019.
 */

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.entity.Department;
import am.egs.hibernate.entity.Employee;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DepartmentDAOImpl implements DepartmentDAO {

  //@Autowired
  //private SessionFactory sessionFactory;

  @Autowired
  private SessionFactory myHibernateSessionFactory;


  public void addDepartment(Department department) {

    Session session = null;
    Transaction transaction = null;
    try {
      session = myHibernateSessionFactory.openSession();
      transaction = session.getTransaction();
      transaction.begin();

      //myHibernateSessionFactory.openSession().saveOrUpdate(department);
      session.persist(department);

      transaction.commit();
    } catch (Exception e) {
      if (transaction != null) {
        transaction.rollback();
      }
      e.printStackTrace();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }

  @SuppressWarnings("unchecked")
  public List<Department> getAllDepartment() {
    Query query = myHibernateSessionFactory.openSession().createQuery("from Department");

    List list = query.list();
    myHibernateSessionFactory.close();

    return list;
  }

  @Override
  public void deleteDepartment(Integer departmentId) {
    Department department = (Department) myHibernateSessionFactory.getCurrentSession().load(
        Department.class, departmentId);
    if (null != department) {
      myHibernateSessionFactory.openSession().delete(department);
    }

  }

  public Department getDepartment(int depid) {
    return (Department) myHibernateSessionFactory.openSession().get(
        Department.class, depid);
  }

  @Override
  public Department updateDepartment(Department department) {
    myHibernateSessionFactory.getCurrentSession().update(department);
    return department;
  }

}

