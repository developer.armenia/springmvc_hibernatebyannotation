package am.egs.hibernate.bean;

/**
 * Created by haykh on 5/23/2019.
 */
public class EmployeeBean {
  private long id;
  private String name;
  private DepartmentBean department;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public DepartmentBean getDepartment() {
    return department;
  }

  public void setDepartment(DepartmentBean department) {
    this.department = department;
  }
}
